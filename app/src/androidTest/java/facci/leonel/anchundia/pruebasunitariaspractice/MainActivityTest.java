package facci.leonel.anchundia.pruebasunitariaspractice;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import org.junit.Rule;
import org.junit.Test;

public class MainActivityTest {
    public static final String correo1="leonel@gmail.com";
    public static final String usuario1="";
    public static final String correo2="leonelgmail.com";
    public static final String usuario2="leonel";
    public static final String correo3="";
    public static final String usuario3="leonel";

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void primera(){
        onView(withId(R.id.email)).perform(typeText(correo1),closeSoftKeyboard());
        onView(withId(R.id.user)).perform(typeText(usuario1),closeSoftKeyboard());
        onView(withId(R.id.bntEnviar)).perform(click());
    }


    @Test
    public void segundo(){
        onView(withId(R.id.email)).perform(typeText(correo2),closeSoftKeyboard());
        onView(withId(R.id.user)).perform(typeText(usuario2),closeSoftKeyboard());
        onView(withId(R.id.bntEnviar)).perform(click());
    }


    @Test
    public void tercero(){
        onView(withId(R.id.email)).perform(typeText(correo3),closeSoftKeyboard());
        onView(withId(R.id.user)).perform(typeText(usuario3),closeSoftKeyboard());
        onView(withId(R.id.bntEnviar)).perform(click());
    }
}
