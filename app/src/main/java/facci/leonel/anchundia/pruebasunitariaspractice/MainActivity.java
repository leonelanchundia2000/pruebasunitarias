package facci.leonel.anchundia.pruebasunitariaspractice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import facci.leonel.anchundia.pruebasunitariaspractice.Validaciones.Email;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    private EditText correo, usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        correo=findViewById(R.id.email);
        usuario=findViewById(R.id.user);
        btn=findViewById(R.id.bntEnviar);
    }
    public void Enviar(View view){
        Vacios();

    }




    private void Vacios() {
        if(Email.email(correo.getText().toString())){
            Toast.makeText(this,"Formato de Correo no valido", Toast.LENGTH_LONG).show();
        }else{
            if(Email.CamposVacios(usuario.getText().toString())){
                Toast.makeText(this, "Campo USUARIO Vacio, colocar su usuario",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"Enviado Correctamente",Toast.LENGTH_LONG).show();
            }
        }
    }

//    private void Vacios (){
//        if(correo.getText().toString().isEmpty()){
//            Toast.makeText(this, "campo de Correo vacio", Toast.LENGTH_LONG).show();
//        }else{
//            if(usuario.getText().toString().isEmpty()){
//                Toast.makeText(this, "campo de Usuario vacio", Toast.LENGTH_LONG).show();
//            }else{
//                Toast.makeText(this, "Enviado Correctamente", Toast.LENGTH_LONG).show();
//            }
//        }
//    }
}